package mx.com.jivan.examen.gapsi.Util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class MiConexionBD extends SQLiteOpenHelper {
	private static MiConexionBD miConexionBD;
	private final String TAG = MiConexionBD.class.getSimpleName();
	private Context context;

	private final String sqlCreateHistorial = "CREATE TABLE IF NOT EXISTS historial(id_historial INTEGER PRIMARY KEY AUTOINCREMENT, busqueda TEXT)";

	private final String sql_page_size = "PRAGMA page_size = 4096;";
	private final String sql_foreign_keys = "PRAGMA foreign_keys = ON;";

	private MiConexionBD(Context context, String name, CursorFactory factory, int version) {

		super(context, name, factory, version);
        this.context =  context;
		Log.i("MiConexionBD ","version "+version+"");
	}

	public static MiConexionBD getInstance(Context ctx){
		if(miConexionBD == null)
		{
			try{
				PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
				miConexionBD = new MiConexionBD(ctx, "UserDan", null, packageInfo.versionCode);
			}catch (Exception e){
				e.printStackTrace();
			}


		}
		return miConexionBD;
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		// TODO Auto-generated method stub
        Log.i("onCreateDatabase", "**DataBase**");
		db.execSQL(sql_page_size);
		db.execSQL(sqlCreateHistorial);
		db.execSQL(sql_foreign_keys);

        Log.i("onCreateTable",sqlCreateHistorial);
		//Log.i("?",TAG.toString());
        String query = "SELECT COUNT(*) FROM historial";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst())
        {
            int count = cursor.getInt(0);

			Log.d(Constants.TAG, "onCreate: " + count);
        }


	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {

	}



	public static int getHistorial(String busquedaKey){
		try{
			SQLiteDatabase db = miConexionBD.getReadableDatabase();
			String query="SELECT * FROM historial WHERE busqueda = '"+busquedaKey+"'";

			Cursor c =db.rawQuery(query, null);
			if(c!=null){
				return c.getCount();
			}
			else
				return 0;

		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}


    public static boolean deleteHistorial(int id){
        boolean var = false;
        try{
            SQLiteDatabase db = miConexionBD.getReadableDatabase();
            String query = "DELETE FROM historial WHERE id_historial = "+ id;

            db.execSQL(query);
            var = true;

			Log.d(Constants.TAG, "Delete historial" + query);
        }catch(Exception e){
            e.printStackTrace();

			Log.d(Constants.TAG, "delete historial Error: " + e.getMessage());
        }
        return var;
    }


	public static boolean insertHistorial(String busqueda){
		try{
			SQLiteDatabase db = miConexionBD.getWritableDatabase();
			db.execSQL("INSERT INTO historial (busqueda) " +
					"VALUES ('" +busqueda +"')");
			//db.close();
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}



    public static boolean clearHistorial(){
        boolean var = false;
        try{
            SQLiteDatabase db = miConexionBD.getReadableDatabase();
            String query = "DELETE FROM historial";
//			Logger.info(TAG, "deleteRowsMessages", query, query);
            db.execSQL(query);
            var = true;


			Log.d(Constants.TAG, "Delete historial: " + query);
        }catch(Exception e){
            e.printStackTrace();

			Log.d(Constants.TAG, "delete historial Error:" + e.getMessage());
		}
		return var;
    }

    public static List<Historial> getHistorial(){
        try{

			SQLiteDatabase db = miConexionBD.getReadableDatabase();
            String query = "SELECT * FROM historial ORDER BY id_historial ";
            Cursor c =db.rawQuery(query, null);
            if(c!=null){
                if(c.moveToFirst()){
                    List<Historial> lista = new ArrayList<Historial>();
                    do{
						Historial historial =  new Historial();

						int id = c.getInt(c.getColumnIndex("id_historial"));
						String busqueda  = c.getString(c.getColumnIndex("busqueda"));

						historial.setId(id);
						historial.setBusqueda(busqueda);


                        lista.add(historial);

                    }while (c.moveToNext());
                    return lista;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


	public static int getMax(){

		int max = 0;

		try{
			SQLiteDatabase db = miConexionBD.getReadableDatabase();
			String query="SELECT MAX(id_historial) FROM historial";

			Cursor c =db.rawQuery(query, null);
			if(c!=null){
				if(c.moveToFirst()){

					int id = c.getInt(0);

					max = id;
					return id;
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		return max;
	}



}
