package mx.com.jivan.examen.gapsi.Principal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import mx.com.jivan.examen.gapsi.Dagger.BaseActivity;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.view.ListadoFragment;
import mx.com.jivan.examen.gapsi.R;
import mx.com.jivan.examen.gapsi.Util.Constants;

public final class PrincipalActivity extends BaseActivity
{

    Toolbar topMenu;
    ImageView btnAtras;
    ImageView btnSig;
    Context contexto;
    Activity actividad;
    private final static String CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath";
    private final static String CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI";
    private String mCurrentPhotoPath = null;
    private Uri mCapturedImageURI = null;
    SwipeRefreshLayout swipeRefreshLayout;
    int menuAct=0;
    boolean inicializado=false;

    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed;

    RelativeLayout splash;

    ListadoFragment perfilFr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal_activity);

        contexto=this;
        actividad=this;

        if (savedInstanceState == null)
        {

            perfilFr = new ListadoFragment();

            addFragment(R.id.fragment_container_plan, perfilFr);

        }




        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Constants.loadIntent=false;

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {




                topMenu = findViewById(R.id.toolbar);
                btnAtras = findViewById(R.id.btn_atras);
                btnSig = findViewById(R.id.btn_siguiente);





                btnAtras.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // TODO Auto-generated method stub


                       perfilFr.goAtras();


                    }
                });


                btnSig.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // TODO Auto-generated method stub


                        perfilFr.goSig();


                    }
                });












                inicializado=true;



            }
        }, 500);



    }
















    public void goPerfil()
    {

        FrameLayout frame = findViewById(R.id.fragment_container_plan);
        frame.removeAllViews();



        addFragment(R.id.fragment_container_plan, new ListadoFragment());
        menuAct=0;
        if(swipeRefreshLayout != null)
            swipeRefreshLayout.setEnabled(false);
    }



    @Override
    public void onBackPressed()
    {
        DrawerLayout navDrawer = findViewById(R.id.drawer_layout);

        if(navDrawer != null) {
            navDrawer.closeDrawer(Gravity.LEFT);
        }
        else
            Log.d(Constants.TAG, "No hay objeto para menu");



        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis())
        {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Salir")
                    .setMessage("¿Desea salir de la aplicación?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            Log.d(Constants.TAG, "Salimos de la App");


                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else
        {
        }

        mBackPressed = System.currentTimeMillis();

    }







    @Override
    public void onResume()
    {
        if(inicializado == true)
            splash.setVisibility(View.GONE);


        super.onResume();

    }


    @Override
    public void onPause()
    {
        if(inicializado == true && Constants.loadIntent == false)
            splash.setVisibility(View.VISIBLE);

        super.onPause();

    }


    public void viewSig(boolean val)
    {
        if(val == true)
            btnSig.setVisibility(View.VISIBLE);
        else
            btnSig.setVisibility(View.INVISIBLE);
    }

    public void viewAtras(boolean val)
    {
        if(val == true)
            btnAtras.setVisibility(View.VISIBLE);
        else
            btnAtras.setVisibility(View.INVISIBLE);
    }



}
