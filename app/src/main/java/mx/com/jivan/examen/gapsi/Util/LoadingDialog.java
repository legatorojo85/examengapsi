package mx.com.jivan.examen.gapsi.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import mx.com.jivan.examen.gapsi.R;

public class LoadingDialog {

    Context contexto;
    Dialog dialog;




    public void showDialog(final Activity activity){
        dialog = new Dialog(activity);




        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        //dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_fragment_loading);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        contexto=activity;


        dialog.show();


    }

    public void dimmis()
    {
        dialog.dismiss();
    }



}