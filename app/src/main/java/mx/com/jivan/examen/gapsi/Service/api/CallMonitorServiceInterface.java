/**
 * Creado por Iván Flores Abril/2019.
 */
package mx.com.jivan.examen.gapsi.Service.api;

import io.reactivex.Observable;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductosResponseEnvelope;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface CallMonitorServiceInterface {

    @Headers({"Content-Type: application/rest+json;charset=UTF-8;"})
    @GET("/appclienteservices/services/v3/plp/")
    Observable<ProductosResponseEnvelope> getProductos(@Query("page-number") int offset,
                                                       @Query("number-of-items-per-page") int limit,
                                                       @Query("search-string") String search,
                                                       @Query("force-plp") boolean plp);





}
