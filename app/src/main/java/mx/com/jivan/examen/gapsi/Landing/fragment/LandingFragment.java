package mx.com.jivan.examen.gapsi.Landing.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;


import javax.inject.Inject;

import mx.com.jivan.examen.gapsi.Dagger.view.BaseFragment;
import mx.com.jivan.examen.gapsi.Principal.PrincipalActivity;
import mx.com.jivan.examen.gapsi.R;
import mx.com.jivan.examen.gapsi.Util.Constants;

public final class LandingFragment extends BaseFragment  {






    @Inject
    LandingFragmentListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(Constants.TAG, "Se crea el fragmento Landing fragment");


        return inflater.inflate(R.layout.landing_ini_fragment, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            Log.d(Constants.TAG, "Se termina la creacion del fragmento Landing fragment, procedemos a cargar Inicial");




            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {


                    Intent intent = new Intent(getActivity(), PrincipalActivity.class);
                    getActivity().startActivity(intent);
                    getActivity().finish();



                }
            }, 1000);







        }

    }

}
