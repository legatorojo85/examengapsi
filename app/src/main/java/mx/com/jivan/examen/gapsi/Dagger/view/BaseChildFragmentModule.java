package mx.com.jivan.examen.gapsi.Dagger.view;

import dagger.Module;


@Module
public abstract class BaseChildFragmentModule {

    public static final String CHILD_FRAGMENT = "BaseChildFragmentModule.childFragment";
}
