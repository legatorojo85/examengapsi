package mx.com.jivan.examen.gapsi.Dagger.util;

import android.app.Fragment;


import javax.inject.Inject;
import javax.inject.Named;

import mx.com.jivan.examen.gapsi.Dagger.view.BaseFragmentModule;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;


@PerFragment
public final class PerFragmentUtil {

    private final Fragment fragment;

    @Inject
    PerFragmentUtil(@Named(BaseFragmentModule.FRAGMENT) Fragment fragment) {
        this.fragment = fragment;
    }

    public String doSomething() {
        return "PerFragmentUtil: " + hashCode() + ", Fragment: " + fragment.hashCode();
    }
}
