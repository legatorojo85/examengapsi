package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.OnClick;
import mx.com.jivan.examen.gapsi.Dagger.view.BaseViewFragment;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.adapter.ProductosListAdapter;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.presenter.ListadoPresenter;
import mx.com.jivan.examen.gapsi.Principal.PrincipalActivity;
import mx.com.jivan.examen.gapsi.R;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductoLiverpool;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductosResponseEnvelope;
import mx.com.jivan.examen.gapsi.Util.Constants;
import mx.com.jivan.examen.gapsi.Util.Functions;
import mx.com.jivan.examen.gapsi.Util.Historial;
import mx.com.jivan.examen.gapsi.Util.LoadingDialog;
import mx.com.jivan.examen.gapsi.Util.MiConexionBD;

public final class ListadoFragment extends BaseViewFragment<ListadoPresenter> implements ListadoView {


    @BindView(R.id.productos_recycler)
    RecyclerView productosRecycler;

    @BindView(R.id.buscar)
    EditText buscarTxt;

    @BindView(R.id.recientes)
    Spinner recientesSpinner;




    private LoadingDialog loadingDialog;
    ProductosResponseEnvelope valores;
    String lastBusqueda = "";




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(Constants.TAG, "Se crea el fragmento Perfil fragment");




        return inflater.inflate(R.layout.listado_fragment, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        final Context contexto=getActivity();

        Log.i(Constants.TAG, "Setting screen name: home" );







        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {



                llenaSpinner();

                recientesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                        Log.d(Constants.TAG, "Value: " + myPosition + "/" + recientesSpinner.getItemAtPosition(myPosition).toString());

                        if(!recientesSpinner.getItemAtPosition(myPosition).toString().equals("Recientes")) {
                            buscarTxt.setText(recientesSpinner.getItemAtPosition(myPosition).toString());
                            Constants.offsetPok=1;
                            buscar();
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });




                //buscar();










            }
        }, 100);




    }

    @OnClick(R.id.btnBusc)
    void goBuscar()
    {
        Constants.offsetPok=1;
        buscar();
    }

    @Override
    public void onResume() {


        super.onResume();
    }

    public void iniciaProceso()
    {

        Log.d(Constants.TAG, "Iniciamos request");

        showView(DisplayView.LOADING_VIEW);


        if(Constants.conexionServer ==  0)
        {
            showView(DisplayView.FORM_VIEW);
            Functions.showSnackBar(getString(R.string.txt_titulo_noint), getActivity(), getActivity());

        }
        else if(Constants.conexionServer ==  2)
        {
            showView(DisplayView.FORM_VIEW);
            Functions.showSnackBar(getString(R.string.txt_titulo_noint), getActivity(), getActivity());

        }

        else
        {

            if(buscarTxt.getText().toString().equals(""))
                lastBusqueda = "gato";
            else
                lastBusqueda = buscarTxt.getText().toString();


            if(MiConexionBD.getInstance(getContext()).getHistorial(lastBusqueda) <= 0)
            {
                MiConexionBD.getInstance(getContext()).insertHistorial(lastBusqueda);
                llenaSpinner();
            }

            presenter.goGetProductos(lastBusqueda);
        }
    }

    public void cargaInicialFin(ProductosResponseEnvelope data)
    {

        Log.d(Constants.TAG, "Iniciamos carga de datos");
        showView(DisplayView.FORM_VIEW);

        valores = data;


        if(data.getStatus().getStatusCode() == 0)
        {
            if(data == null || data.getPlpResults() == null || data.getPlpResults().getRecords() == null)
            {

                Log.d(Constants.TAG, "Nulo");
            }
            else
            {

                if(Constants.offsetPok == 1)
                {
                    ((PrincipalActivity)getActivity()).viewAtras(false);
                }
                else
                {
                    ((PrincipalActivity)getActivity()).viewAtras(true);
                }

                if(((data.getPlpResults().getPlpState().getTotalNumRecs()/Constants.limitPok) - Constants.offsetPok*Constants.limitPok) <= Constants.limitPok)
                {
                    ((PrincipalActivity)getActivity()).viewSig(false);
                }
                else
                {
                    ((PrincipalActivity)getActivity()).viewSig(true);
                }

                List<ProductoLiverpool> productosList = new ArrayList<>();

                ProductosListAdapter productosListAdapter = new ProductosListAdapter(productosList, getActivity().getApplicationContext());

                productosRecycler.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
                productosRecycler.setItemAnimator(new DefaultItemAnimator());



                productosRecycler.setAdapter(productosListAdapter);

                Log.d(Constants.TAG, "Count: " + data.getPlpResults().getRecords().size());
                for (int i=0;i<data.getPlpResults().getRecords().size();i++)
                {

                    productosList.add(data.getPlpResults().getRecords().get(i));
                }

                productosListAdapter.notifyDataSetChanged();

            }

        }
        else
        {
            Functions.showSnackBar(getString(R.string.txt_titulo_noser), getActivity(), getActivity());
        }






    }









    @Override
    public void showView(DisplayView view) {
        switch (view) {
            case FORM_VIEW:
                if (loadingDialog != null) {
                    loadingDialog.dimmis();
                }
                break;
            case LOADING_VIEW:
                loadingDialog = new LoadingDialog();
                loadingDialog.showDialog(getActivity());
                break;
        }
    }

    @Override
    public void showMessage(String message, String titulo)
    {
        showView(DisplayView.FORM_VIEW);



    }


    @Override
    public void muestraAlerta(String titulo, String message)
    {
        showView(DisplayView.FORM_VIEW);



    }

    public void goOut()
    {
        showView(DisplayView.FORM_VIEW);

    }



    public void goSig()
    {



        Constants.offsetPok = Constants.offsetPok + 1;


        Log.d(Constants.TAG, "Empezamos validacion de internet");
        buscar();


    }

    public void goAtras()
    {

        Constants.offsetPok = Constants.offsetPok - 1;


        Log.d(Constants.TAG, "Empezamos validacion de internet");
        buscar();

    }

    public void llenaSpinner()
    {

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Recientes");


        List<Historial> historial = MiConexionBD.getInstance(getContext()).getHistorial();
        if(historial != null)
        {
            for (int i = 0; i < historial.size(); i++) {
                spinnerArray.add(historial.get(i).getBusqueda());
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item, spinnerArray );
        recientesSpinner.setAdapter(spinnerArrayAdapter);
        spinnerArrayAdapter.notifyDataSetChanged();
    }

    public void buscar()
    {
        Functions.hayConexion(getActivity(), new Callable<Void>() {
            public Void call() {
                iniciaProceso();
                return null;
            }
        });
    }


}
