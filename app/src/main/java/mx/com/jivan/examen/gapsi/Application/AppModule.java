package mx.com.jivan.examen.gapsi.Application;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;
import mx.com.jivan.examen.gapsi.Landing.LandingActivity;
import mx.com.jivan.examen.gapsi.Landing.LandingActivityModule;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerActivity;
import mx.com.jivan.examen.gapsi.Principal.PrincipalActivity;
import mx.com.jivan.examen.gapsi.Principal.PrincipalActivityModule;

@Module(includes = AndroidInjectionModule.class)
abstract class AppModule {

    @Binds
    @Singleton
    abstract Application application(App app);


    @PerActivity
    @ContributesAndroidInjector(modules = LandingActivityModule.class)
    abstract LandingActivity mainActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = PrincipalActivityModule.class)
    abstract PrincipalActivity planActivityInjector();


}
