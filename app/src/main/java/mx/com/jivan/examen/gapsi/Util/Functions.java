package mx.com.jivan.examen.gapsi.Util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.DisplayCutout;
import android.view.View;
import android.view.WindowInsets;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;



import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.Callable;


import mx.com.jivan.examen.gapsi.R;

import static mx.com.jivan.examen.gapsi.Service.generales.BuildConfig.BASE_URL_DESA;

public class Functions
{




    public static boolean pingToURL(String urlString)
    {

        //String finalUrl=urlString+"T360CallMonitor/services/T360CallMonitorImpl.T360CallMonitorImplHttpSoap12Endpoint/";
        String finalUrl = "https://google.com";

        try
        {
            URL url = new URL(finalUrl);

            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setRequestProperty("User-Agent", "Android Application: 3.0.0");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(60000); // mTimeout is in seconds
            urlc.setReadTimeout(10000);
            urlc.connect();

            int codeRes = urlc.getResponseCode();
            Log.d(Constants.TAG, "Regreso del ping(" + finalUrl + "): " + codeRes);


            if (codeRes == 200)
            {
                return true;
            }
        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            Log.d(Constants.TAG, "Error al hacer el ping(" + finalUrl + "): " + e1.getMessage());
            return false;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Log.d(Constants.TAG, "Error al hacer el ping(" + finalUrl + "): " + e.getMessage());
            return false;
        }


        return false;


    }

    public static boolean isDataConnectionAvailable(Activity actividad)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) actividad.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }




    public static void hayConexion(Activity actividad, Callable<Void> funcionParametro)
    {

        Constants.conexionServer = 0;

        if(!isDataConnectionAvailable(actividad))
        {
            Log.d(Constants.TAG, "Sin conexion a internet");

            try
            {
                funcionParametro.call();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {

            Log.d(Constants.TAG, "Hay conexion a internet");
            Constants.conexionServer = 1;
            try
            {
                funcionParametro.call();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            /*AsyncTask<Void,Void,Boolean> task = new AsyncTask<Void,Void,Boolean>(){
                @Override
                protected Boolean doInBackground(Void... voids) {

                    boolean res=false;
                    Log.d(Constants.TAG, "Hacemos ping al servidor");
                    res = pingToURL("http://google.com");
                    return res;
                }

                @Override
                protected void onPostExecute(Boolean res)
                {
                    super.onPostExecute(res);
                    if(res == true)
                        Constants.conexionServer = 1;
                    else
                        Constants.conexionServer = 2;

                    try
                    {
                        funcionParametro.call();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }
            };

            task.execute();*/

        }

    }




    public static void showSnackBar(String mensaje, Context contexto, Activity vista)
    {
        Log.d(Constants.TAG, "Mostramos el Mensaje: " + mensaje);

        Animation animation;
        animation = AnimationUtils.loadAnimation(contexto, R.anim.snackbar_enter);


        LinearLayout bloqueMensajeSnackbar = vista.findViewById(R.id.bloque_mensaje_snackbar);
        TextView mensajeSnackbar = vista.findViewById(R.id.mensaje_snackbar);

        mensajeSnackbar.setText(mensaje);
        bloqueMensajeSnackbar.setAnimation(animation);
        bloqueMensajeSnackbar.setVisibility(View.VISIBLE);


        Vibrator v = (Vibrator) contexto.getSystemService(Context.VIBRATOR_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(300);
        }

        Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {

                Animation animationout;
                animationout = AnimationUtils.loadAnimation(contexto, R.anim.snackbar_out);

                bloqueMensajeSnackbar.setAnimation(animationout);

            }
        }, 1300);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                bloqueMensajeSnackbar.setVisibility(View.GONE);

            }
        }, 1600);
    }



}
