package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Domain;

import dagger.Binds;
import dagger.Module;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Data.Repository.ProductosRepositoryModule;

@Module(includes = {
        ProductosRepositoryModule.class
})
public abstract class ProductosInteractorModule {

    @Binds
    @PerFragment
    abstract ProductosInteractor productosInteractor(ProductosInteractorImpl productosInteractorImpl);
}
