package mx.com.jivan.examen.gapsi.Dagger.util;

import android.app.Fragment;

import javax.inject.Inject;
import javax.inject.Named;

import mx.com.jivan.examen.gapsi.Dagger.view.BaseChildFragmentModule;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerChildFragment;

@PerChildFragment
public final class PerChildFragmentUtil {

    private final Fragment childFragment;

    @Inject
    PerChildFragmentUtil(@Named(BaseChildFragmentModule.CHILD_FRAGMENT) Fragment childFragment) {
        this.childFragment = childFragment;
    }

    public String doSomething() {
        return "PerChildFragmentUtil: " + hashCode()
                + ", child Fragment: " + childFragment.hashCode();
    }
}
