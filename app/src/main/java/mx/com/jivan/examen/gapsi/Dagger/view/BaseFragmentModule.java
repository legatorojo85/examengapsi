package mx.com.jivan.examen.gapsi.Dagger.view;

import android.app.Fragment;
import android.app.FragmentManager;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;

@Module
public abstract class BaseFragmentModule {

    public static final String FRAGMENT = "BaseFragmentModule.fragment";

    static final String CHILD_FRAGMENT_MANAGER = "BaseFragmentModule.childFragmentManager";

    @Provides
    @Named(CHILD_FRAGMENT_MANAGER)
    @PerFragment
    static FragmentManager childFragmentManager(@Named(FRAGMENT) Fragment fragment) {
        return fragment.getChildFragmentManager();
    }
}
