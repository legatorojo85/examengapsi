package mx.com.jivan.examen.gapsi.Dagger.util;

import android.app.Activity;


import javax.inject.Inject;

import mx.com.jivan.examen.gapsi.Dagger.inject.PerActivity;

@PerActivity
public final class PerActivityUtil {

    private final Activity activity;

    @Inject
    PerActivityUtil(Activity activity) {
        this.activity = activity;
    }

   public String doSomething() {
        return "PerActivityUtil: " + hashCode() + ", TakePhotoActivity: " + activity.hashCode();
    }
}
