package mx.com.jivan.examen.gapsi.Principal;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import mx.com.jivan.examen.gapsi.Dagger.BaseActivityModule;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerActivity;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.view.ListadoFragment;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.view.ListadoFragmentModule;

@Module(includes = BaseActivityModule.class)
public abstract class PrincipalActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = ListadoFragmentModule.class)
    abstract ListadoFragment perfilFragmentInjector();


    @Binds
    @PerActivity
    abstract Activity activity(PrincipalActivity principalActivity);

}
