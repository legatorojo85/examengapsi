package mx.com.jivan.examen.gapsi.Landing.fragment;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import mx.com.jivan.examen.gapsi.Dagger.view.BaseFragmentModule;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;

@Module(includes = BaseFragmentModule.class)
public abstract class LandingFragmentModule {

    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(LandingFragment landingFragment);
}
