package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Data.Repository;

import dagger.Binds;
import dagger.Module;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;

@Module
public abstract class ProductosRepositoryModule {

    @Binds
    @PerFragment
    abstract ProductosRepository productosRepository(ProductosRepositoryImpl productosRepositoryImpl);
}
