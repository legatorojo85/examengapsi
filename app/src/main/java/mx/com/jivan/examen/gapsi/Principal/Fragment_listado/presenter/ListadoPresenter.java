package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.presenter;

import mx.com.jivan.examen.gapsi.Dagger.presenter.Presenter;

public interface ListadoPresenter extends Presenter {



    void goGetProductos(String busqueda);

}
