package mx.com.jivan.examen.gapsi.Landing;

import android.os.Bundle;
import android.support.annotation.Nullable;


import mx.com.jivan.examen.gapsi.Dagger.BaseActivity;
import mx.com.jivan.examen.gapsi.Landing.fragment.LandingFragment;
import mx.com.jivan.examen.gapsi.Landing.fragment.LandingFragmentListener;
import mx.com.jivan.examen.gapsi.R;
import mx.com.jivan.examen.gapsi.Util.Constants;



public final class LandingActivity extends BaseActivity implements LandingFragmentListener {

    private Bundle bundle = null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.landing_activity);



        if (savedInstanceState == null) {
            addFragment(R.id.fragment_container_landing, new LandingFragment());
        }





    }





    @Override
    public void goInicial()
    {
        //navigator.toInicial(this);
    }



}
