package mx.com.jivan.examen.gapsi.Service.response.productos;

import com.google.gson.annotations.SerializedName;

public class StatusResponse {

    @SerializedName("statusCode")
    private int statusCode;

    @SerializedName("status")
    private String status;

    @SerializedName("errorDescription")
    private String errorDescription;









    public int getStatusCode()
    {
        return statusCode;
    }

    public void setStatusCode(int statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getErrorDescription()
    {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription)
    {
        this.errorDescription = errorDescription;
    }



}
