package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.view;


import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;
import mx.com.jivan.examen.gapsi.Dagger.view.BaseFragmentModule;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.presenter.ListadoPresenterModule;

@Module(includes = {
        BaseFragmentModule.class,
        ListadoPresenterModule.class
})
public abstract class ListadoFragmentModule {

    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(ListadoFragment listadoFragment);

    @Binds
    @PerFragment
    abstract ListadoView perfilView(ListadoFragment listadoFragment);

}
