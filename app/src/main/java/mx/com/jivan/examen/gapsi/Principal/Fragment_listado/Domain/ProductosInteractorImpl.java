package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Domain;

import javax.inject.Inject;

import io.reactivex.Observable;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Data.Repository.ProductosRepository;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Data.Repository.ProductosRepositoryImpl;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductosResponseEnvelope;

public class ProductosInteractorImpl implements ProductosInteractor {

    private ProductosRepository productosRepository;

    @Inject
    public ProductosInteractorImpl(ProductosRepositoryImpl confirmSigInLiteRepositoryImpl){
        this.productosRepository = confirmSigInLiteRepositoryImpl;
    }

    @Override
    public Observable<ProductosResponseEnvelope> doGetProductos(String busqueda)
    {
        return productosRepository.doGetProductos(busqueda);
    }
}
