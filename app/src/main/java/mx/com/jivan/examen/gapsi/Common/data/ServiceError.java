package mx.com.jivan.examen.gapsi.Common.data;

public class ServiceError extends Throwable{
    private int code;
    private String message;

    public ServiceError(int code, String message){
        this.code = code;
        this.message = message;
    }
}
