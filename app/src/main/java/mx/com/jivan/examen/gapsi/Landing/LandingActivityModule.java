package mx.com.jivan.examen.gapsi.Landing;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import mx.com.jivan.examen.gapsi.Dagger.BaseActivityModule;
import mx.com.jivan.examen.gapsi.Landing.fragment.LandingFragment;
import mx.com.jivan.examen.gapsi.Landing.fragment.LandingFragmentListener;
import mx.com.jivan.examen.gapsi.Landing.fragment.LandingFragmentModule;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerActivity;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;

@Module(includes = BaseActivityModule.class)
public abstract class LandingActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = LandingFragmentModule.class)
    abstract LandingFragment landingFragmentInjector();

    @Binds
    @PerActivity
    abstract Activity activity(LandingActivity landingActivity);

    @Binds
    @PerActivity
    abstract LandingFragmentListener landingFragmentListener(LandingActivity landingActivity);

}
