package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import mx.com.jivan.examen.gapsi.R;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductoLiverpool;
import mx.com.jivan.examen.gapsi.Util.Constants;

public class ProductosListAdapter extends RecyclerView.Adapter<ProductosListHolder> {

    List<ProductoLiverpool> datos;
    Context context;



    public ProductosListAdapter(List<ProductoLiverpool> datos, Context context) {

        this.datos = datos;
        this.context = context;

    }

    @NonNull
    @Override
    public ProductosListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.productos_list_layout, parent, false);

        return new ProductosListHolder(view);
    }

    public void onBindViewHolder(@NonNull ProductosListHolder holder, int position)
    {

        ProductoLiverpool model = datos.get(position);

        holder.nombre.setText(model.getProductDisplayName());
        holder.precio.setText("$" + model.getListPrice());


        if(model.getSmImage() != null)
        {
            Picasso.with(context).load(model.getSmImage()).into(holder.imagen);
        }

    }


    @Override
    public int getItemCount() {
        return datos == null ? 0 : datos.size();
    }
}
