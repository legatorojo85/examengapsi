package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Domain;

import io.reactivex.Observable;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductosResponseEnvelope;

public interface ProductosInteractor {

    Observable<ProductosResponseEnvelope> doGetProductos(String busqueda);
}
