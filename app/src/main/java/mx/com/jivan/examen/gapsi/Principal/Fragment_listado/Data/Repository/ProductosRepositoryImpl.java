package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Data.Repository;

import javax.inject.Inject;

import io.reactivex.Observable;
import mx.com.jivan.examen.gapsi.Service.api.CallMonitorServiceInterface;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductosResponseEnvelope;
import mx.com.jivan.examen.gapsi.Util.Constants;

public class ProductosRepositoryImpl implements ProductosRepository {

    private CallMonitorServiceInterface callMonitorService;

    @Inject
    public ProductosRepositoryImpl(CallMonitorServiceInterface callMonitorService){
        this.callMonitorService = callMonitorService;
    }


    @Override
    public Observable<ProductosResponseEnvelope> doGetProductos(String busqueda)
    {
        return callMonitorService.getProductos(Constants.offsetPok, Constants.limitPok, busqueda, true).flatMap(response -> {
            ProductosResponseEnvelope responseProductos = new ProductosResponseEnvelope();

            responseProductos.setStatus(response.getStatus());
            if(response.getPlpResults() != null)
                responseProductos.setPlpResults(response.getPlpResults());
            if(response.getPageType() != null)
                responseProductos.setPageType(response.getPageType());
            /**Asignaciones**/

            return Observable.just(responseProductos);


        });
    }
}
