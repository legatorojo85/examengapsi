package mx.com.jivan.examen.gapsi.Application;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import mx.com.jivan.examen.gapsi.Service.module.RetrofitModule;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class})
interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App> {
    }

}