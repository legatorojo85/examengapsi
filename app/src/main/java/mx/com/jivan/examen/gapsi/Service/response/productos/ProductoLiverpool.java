package mx.com.jivan.examen.gapsi.Service.response.productos;

import com.google.gson.annotations.SerializedName;

public class ProductoLiverpool {

    @SerializedName("productId")
    private long productId;

    @SerializedName("productDisplayName")
    private String productDisplayName;

    @SerializedName("listPrice")
    private float listPrice;

    @SerializedName("smImage")
    private String smImage;







    public long getProductId()
    {
        return productId;
    }

    public void setProductId(long productId)
    {
        this.productId = productId;
    }

    public String getProductDisplayName()
    {
        return productDisplayName;
    }

    public void setProductDisplayName(String productDisplayName)
    {
        this.productDisplayName = productDisplayName;
    }

    public float getListPrice()
    {
        return listPrice;
    }

    public void setListPrice(float listPrice)
    {
        this.listPrice = listPrice;
    }

    public String getSmImage()
    {
        return smImage;
    }

    public void setSmImage(String smImage)
    {
        this.smImage = smImage;
    }
}
