package mx.com.jivan.examen.gapsi.Service.response.productos;


import com.google.gson.annotations.SerializedName;

public class ProductosResponseEnvelope {


    @SerializedName("status")
    private StatusResponse status;

    @SerializedName("pageType")
    private String pageType;

    @SerializedName("plpResults")
    private ProductosResults plpResults;

















    public StatusResponse getStatus()
    {
        return status;
    }

    public void setStatus(StatusResponse status)
    {
        this.status = status;
    }

    public String getPageType()
    {
        return pageType;
    }

    public void setPageType(String pageType)
    {
        this.pageType = pageType;
    }

    public ProductosResults getPlpResults()
    {
        return plpResults;
    }

    public void setPlpResults(ProductosResults plpResults)
    {
        this.plpResults = plpResults;
    }

}
