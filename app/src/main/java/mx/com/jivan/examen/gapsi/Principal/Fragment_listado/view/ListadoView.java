package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.view;

import mx.com.jivan.examen.gapsi.Dagger.view.MVPView;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductosResponseEnvelope;

public interface ListadoView extends MVPView {

    void showMessage(String message, String titulo);

    void muestraAlerta(String message, String titulo);

    void showView(DisplayView view);

    void goOut();


    void cargaInicialFin(ProductosResponseEnvelope data);

    enum DisplayView {
        FORM_VIEW, LOADING_VIEW
    }


}
