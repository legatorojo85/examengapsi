package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Data.Repository;

import io.reactivex.Observable;
import mx.com.jivan.examen.gapsi.Service.response.productos.ProductosResponseEnvelope;

public interface ProductosRepository {


    Observable<ProductosResponseEnvelope> doGetProductos(String busqueda);

}
