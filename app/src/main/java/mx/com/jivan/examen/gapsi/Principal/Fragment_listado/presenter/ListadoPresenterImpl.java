package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.presenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;
import mx.com.jivan.examen.gapsi.Dagger.presenter.BasePresenter;
import mx.com.jivan.examen.gapsi.Dagger.util.PerActivityUtil;
import mx.com.jivan.examen.gapsi.Dagger.util.PerFragmentUtil;
import mx.com.jivan.examen.gapsi.Dagger.util.SingletonUtil;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Domain.ProductosInteractor;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.view.ListadoView;
import mx.com.jivan.examen.gapsi.Util.Constants;

@PerFragment
final class ListadoPresenterImpl extends BasePresenter<ListadoView> implements ListadoPresenter {

    private final SingletonUtil singletonUtil;
    private final PerActivityUtil perActivityUtil;
    private final PerFragmentUtil perFragmentUtil;
    private final ProductosInteractor productosInteractor;

    @Inject
    ListadoPresenterImpl(ListadoView view, SingletonUtil singletonUtil,
                         PerActivityUtil perActivityUtil, PerFragmentUtil perFragmentUtil,
                         ProductosInteractor productosInteractor) {
        super(view);
        this.singletonUtil = singletonUtil;
        this.perActivityUtil = perActivityUtil;
        this.perFragmentUtil = perFragmentUtil;
        this.productosInteractor = productosInteractor;
    }








    @Override
    public void goGetProductos(String busqueda)
    {
        productosInteractor.doGetProductos(busqueda)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {


                    if(response.getStatus().getStatusCode() == 0)
                        view.cargaInicialFin(response);
                    else
                        view.showMessage("Error", response.getStatus().getErrorDescription());


                }, throwable -> {

                    view.showMessage("Error", "Error desconocido");
                });

    }



}
