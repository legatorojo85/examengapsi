package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import mx.com.jivan.examen.gapsi.R;

public class ProductosListHolder extends RecyclerView.ViewHolder {

    TextView nombre;
    TextView precio;
    ImageView imagen;



    public ProductosListHolder(View itemView) {
        super(itemView);

        nombre = itemView.findViewById(R.id.nombre);
        precio = itemView.findViewById(R.id.precio);
        imagen = itemView.findViewById(R.id.imagen);
    }
}
