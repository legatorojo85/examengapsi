package mx.com.jivan.examen.gapsi.Principal.Fragment_listado.presenter;


import dagger.Binds;
import dagger.Module;
import mx.com.jivan.examen.gapsi.Dagger.inject.PerFragment;
import mx.com.jivan.examen.gapsi.Principal.Fragment_listado.Domain.ProductosInteractorModule;

@Module(includes = ProductosInteractorModule.class)
public abstract class ListadoPresenterModule {


    @Binds
    @PerFragment
    abstract ListadoPresenter perfilPresenter(ListadoPresenterImpl perfilPresenterImpl);

}
