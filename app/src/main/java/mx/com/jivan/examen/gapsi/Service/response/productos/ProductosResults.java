package mx.com.jivan.examen.gapsi.Service.response.productos;

import com.google.gson.annotations.SerializedName;

import org.simpleframework.xml.ElementList;

import java.util.List;

public class ProductosResults {

    @ElementList(required = false, name = "records", inline = true)
    private List<ProductoLiverpool> records;

    @SerializedName("plpState")
    private ProductosPlpState plpState;









    public ProductosPlpState getPlpState()
    {
        return plpState;
    }

    public void setPlpState(ProductosPlpState plpState)
    {
        this.plpState = plpState;
    }

    public List<ProductoLiverpool> getRecords() {
        return records;
    }

    public void setRecords(List<ProductoLiverpool> records)
    {
        this.records = records;
    }

}
