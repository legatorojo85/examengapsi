/**
 * Creado por Iván Flores Abril/2019.
 */


package mx.com.jivan.examen.gapsi.Application;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;


import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import mx.com.jivan.examen.gapsi.Util.Constants;



public class App extends Application implements HasActivityInjector {


    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent.builder().create(this).inject(this);

        context = getApplicationContext();



        Log.d(Constants.TAG, "Entramos a App");


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }





}
